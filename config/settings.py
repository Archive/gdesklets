#
# Module for holding runtime settings.
#


show_tray_icon = True
show_load_splash = False
translucent = False
dpi = 96
editor = "gnome-text-editor"
float_key = (96, 1)  # <Shift> <F12>
